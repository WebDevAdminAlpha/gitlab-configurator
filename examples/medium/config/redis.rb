redis_master_role['enable'] = true
redis['port'] = 6379
redis['bind'] = '0.0.0.0'
redis['password'] = 'redispassword'
gitlab_rails['auto_migrate'] = false
