postgresql['enable'] = false
redis['enable'] = false
nginx['enable'] = false
prometheus['enable'] = false
unicorn['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
grafana['enable'] = false

gitlab_rails['rake_cache_clear'] = false
gitlab_rails['auto_migrate'] = false

gitlab_rails['internal_api_url'] = 'https://gitlab.example.com'

gitaly['auth_token'] = 'abc123secret'
gitlab_shell['secret_token'] = 'itsasecretfromeverybody'

gitaly['tls_listen_addr'] = "0.0.0.0:8076"
gitaly['certificate_path'] = "/etc/gitlab/ssl/PUBLIC_CERTIFICATE"
gitaly['key_path'] = "/etc/gitlab/ssl/PRIVATE_KEY"

git_data_dirs({
  'default' => {
    'path' => '/var/opt/gitlab/git-data'
  }
})
